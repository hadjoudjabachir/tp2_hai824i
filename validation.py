from rdflib import *
from main import getExpression
from rdflib import Graph
import xml.etree.ElementTree as ET

def verite_de_terrain(seuil,source,target):
    # Création d'un graphe RDF
    g = Graph()

    # Charger le contenu du fichier RDF
    g.parse("refDHT.RDF")

    # Initialisation d'une liste pour stocker les informations extraites
    liste_reference = []
    ressource1 = None
    ressource2 = None
    mesure = None

    # Définition de la liste de références
    liste_reference = []

    # Analyse du fichier XML
    tree = ET.parse('refDHT.RDF')
    root = tree.getroot()
    
    # Namespace pour les éléments RDF
    ns = {"rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
      "align": "http://knowledgeweb.semanticweb.org/heterogeneity/alignment"}
    
    # Parcours des éléments "map" dans le fichier RDF
    for map_elem in root.findall(".//align:map", ns):

        # Extraction des entités
        entity1 = map_elem.find("align:Cell/align:entity1", ns).get("{http://www.w3.org/1999/02/22-rdf-syntax-ns#}resource")
        entity2 = map_elem.find("align:Cell/align:entity2", ns).get("{http://www.w3.org/1999/02/22-rdf-syntax-ns#}resource")
        
        
        # Stockage des entités dans la liste
        liste_reference.append([entity1, entity2])

    
    #print("référénce : ", liste_reference)

    with open('resultats.ttl', 'r') as f:
        lines = f.readlines()

    liste_resultats = []
    for line in lines:
        if 'owl:sameAs' in line:
            parts = line.strip().split()
            liste_resultats.append([parts[0][1:-1], parts[2][1:-1]])

   

    #print("résultats : ", liste_resultats)

    VP = 0
    FP = 0
    FN = 0

    # j'essaye de trouver les vrais positifs
    for ref in liste_reference:
       if ref in liste_resultats:
            VP += 1
   #les faux positifs
    for j in liste_resultats:
        if j not in liste_reference:
            FP += 1
    #faux négatifs
    for i in liste_reference:
        if i not in liste_resultats:
            FN += 1

    # vrais négatifs
    g1 = Graph()
    g1.parse(source, format='ttl')
    g2 = Graph()
    g2.parse(target,format='ttl')

    result = getExpression(g1)
    result2 = getExpression(g2)
     # Obtenir une liste de toutes les paires de ressources possibles dans les fichiers source et cible
    pairs = [(s, t) for s in result for t in result2]

    # Initialiser le compteur de vrais négatifs à zéro
    VN = 0

    # Parcourir toutes les paires de ressources possibles
    for pair in pairs:
        # Vérifier si la paire n'est pas dans la liste de référence ni dans la liste de résultats
        if pair not in [r[:2] for r in liste_reference] and pair not in [r[:2] for r in liste_resultats]:
            VN += 1

    # Afficher le nombre de FP VP VN FN
    print("FP =", FP)
    print("VP =", VP)
    print("VN =", VN)
    print("FN =", FN)

    #calcul de la précision, rappel, fmeasure et accuracy et les rajouter au fichier scores.csv
    precision = (VP) / (VP + FP)

    rappel = VP / (VP + FN)

    fmeasure = (2 * precision * rappel)/(rappel + precision)

    accuracy = (VP + VN) / (VP + VN + FP + FN)

    finalCsv = open("scores.csv", "a", encoding="utf-8")
    finalCsv.write("{},{},{},{},{}\n".format(seuil,precision, rappel, fmeasure, accuracy))




if __name__ == '__main__':
    verite_de_terrain(0.5, "source.ttl", "target.ttl")