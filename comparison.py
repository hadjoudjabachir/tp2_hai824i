import py_stringmatching as sm


def identityEqualMeasure(s1, s2):
    return s1 == s2


def levenshtein(s1, s2):
    lev = sm.Levenshtein()
    return lev.get_sim_score(s1, s2)


def jaro(s1, s2):
    jaro = sm.Jaro()
    return jaro.get_sim_score(s1, s2)


def smoa(s1, s2):
    return 0

def ngram(s1, s2, size):
    i = 0
    same = 0

    while ((i + size - 1) < len(s1)) or ((i + size - 1) < len(s2)):
        if s1[i:i + size] == s2[i:i + size]:
            same += 1
        i += 1

    if((min(len(s2), len(s1)) - size + 1) == 0):
        return 0
    return (same) / (min(len(s2), len(s1)) - size + 1)


def jaccard(set1, set2):
    jaccard = sm.Jaccard()
    return jaccard.get_sim_score(set1, set2)


def monge_elkan(set1, set2):
    monge_elkan = sm.MongeElkan()
    return monge_elkan.get_raw_score(set1, set2)
#La méthode de comparaison selon les mesures séléctionnées et qui fait la moyenne pondérée entre les mesures séléctionnées
def compare(s1, s2, identity=1, levenshteinBool=1, jaroBool=1, ngramBool=1, ngram_size=2, jaccardBool=1, monge_elkanBool=1):
    result = 0
    nbMesure = 0
    
    if identity:
        result += identityEqualMeasure(s1.text, s2.text)
        if result:
            return True
    if levenshteinBool:
        result += levenshtein(s1.text, s2.text)*levenshteinBool
        nbMesure += levenshteinBool
    if jaroBool:
        result += jaro(s1.text, s2.text)*jaroBool
        nbMesure += jaroBool
    if ngramBool:
        result += ngram(s1.text, s2.text, ngram_size)*ngramBool
        nbMesure += ngramBool
    if monge_elkanBool or jaccardBool:
        set1 = [token.text for token in s1.tokens]
        set2 = [token.text for token in s2.tokens]
    if jaccardBool:
        result += jaccard(set1, set2)*jaccardBool
        nbMesure += jaccardBool
    if monge_elkanBool:
        result += monge_elkan(set1, set2)*monge_elkanBool
        nbMesure += monge_elkanBool

        

    if(nbMesure==0):
        return 0
    return result/nbMesure


if __name__ == '__main__':

    print(compare("La femme mange une pommme", "une femmme cueille une pomme", 0.5 , identity=True, levenshteinBool=True, jaroBool=True, ngramBool=True, ngram_size=2, jaccardBool=True, monge_elkanBool=True))
