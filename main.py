from rdflib import *
from comparison import compare
import spacy
import concurrent.futures
from rdflib import Graph, Namespace
from rdflib.plugins.sparql import prepareQuery
from rdflib import Namespace,URIRef
import sys
class Contenu:
    def __init__(self, text):
        self.text = text
        self.tokens = nlp(text)

class Expression_Class:
    

    def getClef(self, expression):
        mus=Namespace("http://data.doremus.org/ontology#")
        query= prepareQuery('''
            SELECT ?expression ?key
            WHERE {
                ?expression mus:U11_has_key ?key .
                FILTER (isIRI(?key))
            }
            ''',initNs={"mus": mus})

        result = []
        for row in self.graph.query(query, initBindings={'expression': expression}):
            result.append(str(row.key))

        return result;

    def getTitre(self, expression):
        ecrm = Namespace("http://erlangen-crm.org/current/")
        query = prepareQuery('''
            SELECT ?title
            WHERE {
                ?expression ecrm:P102_has_title ?title .
            }
        ''', initNs={"ecrm": ecrm})

        result = []
        for row in self.graph.query(query, initBindings={"expression": expression}):
            result.append(str(row.title))

        return result;

    def getCompositeur(self, expression):
        ecrm = Namespace("http://erlangen-crm.org/current/")
        efrbroo=Namespace("http://erlangen-crm.org/efrbroo/")
        query = prepareQuery('''
            SELECT ?expression ?composer
            WHERE {
                ?expression a efrbroo:F22_Self-Contained_Expression .
                ?expCreation efrbroo:R17_created ?expression ;
                ecrm:P9_consists_of / ecrm:P14_carried_out_by ?composer ;
            }
        ''',initNs={"efrbroo": efrbroo, "ecrm" : ecrm})

        result = []
        for row in self.graph.query(query, initBindings={"expression": expression}):
            result.append(str(row.composer))

        return result;

    def getGenre(self, expression):
        req = """
                    PREFIX mus: <http://data.doremus.org/ontology#>

                    SELECT ?expression ?genre
                    WHERE {
                      ?expression mus:U12_has_genre ?genre .
                      FILTER (isIRI(?genre))
                    }

                """

        qres = self.graph.query(req, initBindings={'expression': expression})
        result = []

        req2 = """
                    PREFIX mus: <http://data.doremus.org/ontology#>
                    PREFIX ecrm: <http://erlangen-crm.org/current/>

                    SELECT ?expression ?genre
                    WHERE {
                      ?expression mus:U12_has_genre / ecrm:P1_is_identified_by ?genre .
                    }

                """

        qres2 = self.graph.query(req2, initBindings={'expression': expression})

        for row in qres:
            result.append(str(row.genre).split("/")[-1].replace("_", " "))

        for row in qres2:
            result.append(str(row.genre))

        return result

    def getNote(self, expression):
        ecrm = Namespace("http://erlangen-crm.org/current/")
        query = prepareQuery('''
            SELECT ?expression ?note
            WHERE {
            ?expression ecrm:P3_has_note ?note .
            }
        ''',initNs={"ecrm": ecrm})

        result=[]
        for row in self.graph.query(query, initBindings={"expression": expression}):
            result.append(str(row.note))

        return result;

    

    def getOpus(self, expression):
         mus = Namespace("http://data.doremus.org/ontology#")

         query = prepareQuery('''
                    SELECT ?expression ?opus WHERE {
                ?expression mus:U17_has_opus_statement/mus:U42_has_opus_number ?opus .
            }
        ''', initNs={"mus":mus})
        
         result=[]
         for row in self.graph.query(query, initBindings={"expression": expression}):
            result.append(str(row.opus))

         return result;
         


    def __init__(self, graph, expression):
        self.graph = graph
        self.expression = expression
        self.titre, self.note, self.compositeur, self.clef, self.opus, self.genre = {}, {}, {}, {}, {}, {}
        for key in ['clef', 'titre', 'compositeur', 'note', 'opus', 'genre']:
            texts = [Contenu(text) for text in getattr(self, f'get{key.capitalize()}')(expression)]
            setattr(self, key, texts)
    
    def __getitem__(self, key):
        return getattr(self, key)
    #comparer une propriété précise du self avec un type d'une exp2 passée en paramètre
    def type_comparaison(self, exp2, type, identity=1, levenshteinBool=0, jaroBool=0, ngramBool=0, ngram_size=2, jaccardBool=0, monge_elkanBool=0):
        resultat = []
        for e1 in self[type]:
            for e2 in exp2[type]:
                resultat.append(compare(e1, e2, identity=identity, levenshteinBool=levenshteinBool, jaroBool=jaroBool, ngramBool=ngramBool, ngram_size=ngram_size, jaccardBool=jaccardBool, monge_elkanBool=monge_elkanBool))


        if len(resultat) > 0:
            return max(resultat)
        return 0
    #Pour pouvoir comparer plusieurs types en même temps et faire la moyenne des valeurs de retour de mesures de similarités des propriétés séléctionnées 
    def plusieursTypesComparaison(self, exp2, titre=True, genre=True, note=True, compositeur=True, clef=True, opus=True, identity=1, levenshteinBool=1, jaroBool=1, ngramBool=1, ngram_size=2, jaccardBool=1, monge_elkanBool=1):
        result = 0
        nbAttr = clef + titre + genre + compositeur + note + opus

        if clef:
            result += self.type_comparaison(exp2, "clef", identity=identity, levenshteinBool=levenshteinBool, jaroBool=jaroBool, ngramBool=ngramBool, ngram_size=ngram_size, jaccardBool=jaccardBool, monge_elkanBool=monge_elkanBool)
        if titre:
            result += self.type_comparaison(exp2, "titre", identity=identity, levenshteinBool=levenshteinBool, jaroBool=jaroBool, ngramBool=ngramBool, ngram_size=ngram_size, jaccardBool=jaccardBool, monge_elkanBool=monge_elkanBool)
        if genre:
            result += self.type_comparaison(exp2, "genre", identity=identity, levenshteinBool=levenshteinBool, jaroBool=jaroBool, ngramBool=ngramBool, ngram_size=ngram_size, jaccardBool=jaccardBool, monge_elkanBool=monge_elkanBool)
        if compositeur:
            result += self.type_comparaison(exp2, "compositeur", identity=True)
        if note:
            result += self.type_comparaison(exp2, "note", identity=identity, levenshteinBool=levenshteinBool, jaroBool=jaroBool, ngramBool=ngramBool, ngram_size=ngram_size, jaccardBool=jaccardBool, monge_elkanBool=monge_elkanBool)
        if opus:
            result += self.type_comparaison(exp2, "opus", identity=identity, levenshteinBool=levenshteinBool, jaroBool=jaroBool, ngramBool=ngramBool, ngram_size=ngram_size, jaccardBool=jaccardBool, monge_elkanBool=monge_elkanBool)

        return result / nbAttr
    
#Renvoie toutes les expressions du graphe de type efrbroo:F22_Self-Contained_Expression
def getExpression(graph):
    efrbroo = Namespace("http://erlangen-crm.org/efrbroo/")
    query = prepareQuery('''
        SELECT DISTINCT ?expression WHERE {
            ?expression a efrbroo:F22_Self-Contained_Expression .
        }
    ''', initNs={"efrbroo": efrbroo})
    
    results = graph.query(query)
    print("Found {} expressions".format(len(results)))
    return results

#écrire le préfixe en haut du fichier resultats.ttl
def ecrireDebutFichier(file):
    with open(file, "w", encoding="utf-8") as fichierResultat:
        fichierResultat.write('@prefix owl: <http://www.w3.org/2002/07/owl#>.\n')

#ajoute la relation owl:sameAs entre les 2 entités sur le fichier resultats.ttl
def ajoutRelation(entity1, entity2, file):
    fichierResultat = open(file, "a",  encoding="utf-8")
    #ajouter le préfixe au début du fichier resultats.ttl
    #fichierResultat.write("@prefix owl: <http://www.w3.org/2002/07/owl#>.\n")
    fichierResultat.write("<" + entity1 + "> " + "owl:sameAs " + "<" + entity2 + "> .\n")

#Compare une expression du fichier source.ttl avec toutes les expressions du fichier target.ttl
def comparaisonsParalleles(exp1, result2, threshold, titre, genre, note, compositeur, clef, opus, identity, levenshteinBool, jaroBool, ngramBool, ngram_size, jaccardBool, monge_elkanBool):
    print("EXP1: " + str(exp1.expression))
    for index, exp2 in enumerate(result2):
        value = exp1.plusieursTypesComparaison(exp2, titre, genre, note, compositeur, clef, opus, identity, levenshteinBool, jaroBool, ngramBool, ngram_size, jaccardBool, monge_elkanBool)
        if value > threshold: #si valeur supérieure au seuil on la rajoute au resultats.ttl
            ajoutRelation(exp1.expression, exp2.expression, "resultats.ttl")



def main(graph1, graph2, threshold=0.5, titre=True, genre=True, note=True, compositeur=True, clef=True, opus=True, identity=1, levenshteinBool=1, jaroBool=1, ngramBool=1, ngram_size=2, jaccardBool=1, monge_elkanBool=1):
    
    #Récupère les noms des fichiers passés en arguments
    source_file = graph1
    target_file = graph2

    #Crée les graphes RDF
    g1 = Graph()
    g2 = Graph()

    g1.parse(source_file, format='ttl')
    g2.parse(target_file, format='ttl')

    
    global nlp
    nlp = spacy.load("fr_core_news_sm", exclude=["parser", "tagger", "ner"])
    global result2


    fileFinal = "resultats.ttl"
    fileFinal = open(fileFinal, "w", encoding="utf-8")
    
    ecrireDebutFichier(fileFinal.name)
    
    result = [Expression_Class(g1, expression[0]) for expression in getExpression(g1)]
    result2 = [Expression_Class(g2, expression[0]) for expression in getExpression(g2)]

    print("On commence la comparaison")
    # Créer une liste vide pour stocker les résultats des comparaisons futures
    futures_resultats = []

    # Créer un Pool de processus pour exécuter les tâches en parallèle
    with concurrent.futures.ProcessPoolExecutor() as executor:
        # Soumettre chaque tâche à exécuter dans le pool de processus
        for exp1 in result:
            future = executor.submit(comparaisonsParalleles, exp1, result2, threshold, titre, genre, note, compositeur, clef, opus, identity, levenshteinBool, jaroBool, ngramBool, ngram_size, jaccardBool, monge_elkanBool)
            futures_resultats.append(future)

        # Récupérer les résultats de chaque tâche terminée
        for future in concurrent.futures.as_completed(futures_resultats):
            if future.exception() is not None:
                raise future.exception()

    print("On finit la comparaison")
