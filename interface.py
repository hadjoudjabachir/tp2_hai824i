import tkinter as tk
from tkinter import filedialog
import rdflib
from tkinter import *
import customtkinter
from main import main
from validation import verite_de_terrain
import os

class SimilaritySelection:
    def __init__(self, master):
        self.master = master
        master.title("Sélection des mesures de similarité")
        master.resizable(False, False)  # Fenêtre de taille fixe
        self.row = 0

        # Import des fichiers
        self.file1_button = customtkinter.CTkButton(master=root, text="Importer fichier 1", command=self.import_file1, width=15)
        self.file1_button.grid(row=self.row, column=0, padx=10, pady=10)

        self.file2_button = customtkinter.CTkButton(master=root, text="Importer fichier 2", command=self.import_file2, width=15)
        self.file2_button.grid(row=self.row, column=1, padx=10, pady=10)


        # Liste des proprietes
        self.properties = ['Titre', 'Genre', 'Compositeur', 'Clef', 'Note', 'Opus']
        self.properties_list = []
        propertyColumn = 0
        for i, property in enumerate(self.properties):
            #TODO: Pour un affichage plus organisé
            if (i % 2 == 0) :
                propertyRow = self.row+1
                propertyColumn = 0
            else :
                propertyRow = self.row
                propertyColumn = 1

            var = tk.BooleanVar()
            var.set(False)
            self.properties_list.append(var)
            checkbox = customtkinter.CTkCheckBox(master=root, text=property, variable=var)
            checkbox.grid(row=propertyRow, column=propertyColumn, sticky='W', padx=10, pady=5)
            self.row = self.row+1

        #seuil
        self.Seuil_entry = customtkinter.CTkEntry(master=root, placeholder_text="Seuil")
        self.Seuil_entry.grid(row=self.row+1, column=0, padx=10, pady=5)
        self.row = self.row+1

        # Liste des mesures de similarité
        self.similarities = ['jaro', 'identity', 'levenshtein', 'ngram', 'jaccard', 'monge_elkan']
        self.mesure_list = []
        #tableau contenant les poids TODO
        self.weight_entry_list = []
        for i, similarity in enumerate(self.similarities):
            iterRow = self.row+1
            self.row = self.row+1
            var = tk.BooleanVar()
            var.set(False)
            self.mesure_list.append(var)
            checkbox = customtkinter.CTkCheckBox(master=root, text=similarity, variable=var)
            checkbox.grid(row=iterRow, column=0, sticky='W', padx=10, pady=5)
            #TODO variable poids a ajouter au tableau, elle est associ"e au champs de saisie weight_entry
            poids = tk.IntVar()
            poids.set(1)
            self.weight_entry_list.append(poids)
            self.weight_entry_list[i] = customtkinter.CTkEntry(master=root, placeholder_text="1")
            self.weight_entry_list[i].grid(row=iterRow, column=1, padx=10, pady=5)
        self.ngram_size = self.weight_entry_list[i] = customtkinter.CTkEntry(master=root, placeholder_text="ngram_size (si mesure selectionnée)")
        self.ngram_size.grid(row=self.row+1, column=1, padx=10, pady=5)
        self.row = self.row+1

        # Bouton de validation
        self.validate_button = customtkinter.CTkButton(master=root, text="Valider", command=self.validate, width=15)
        self.validate_button.grid(row=self.row+1, column=0, columnspan=2, padx=10, pady=20)
        self.row = self.row+1
    def import_file1(self):
        file_path = filedialog.askopenfilename()
        self.file1_name = os.path.basename(file_path)

    def import_file2(self):
        file_path = filedialog.askopenfilename()
        self.file2_name = os.path.basename(file_path)

    def validate(self):
        selected_similarities = []
        print("Mesures de similarité selectionnées :")
        for i, var in enumerate(self.mesure_list):
            if var.get():
                selected_similarities.append(self.similarities[i])
                print("         "+self.similarities[i])


        print("Appliquer")
        #recuperer les valeurs de poids associées a chaque mesure du tableau
        taille = len(self.similarities)
        calculatedValues = []
        for i in range(0, taille):
            #On calcule l'instruction suivante en iterant sur les variables cochées
            #TODO : si weight_list[i].get() marche pas, essayer weight_list[i].cget()
            v = self.weight_entry_list[i].get() if self.mesure_list[i].get() else 0
            calculatedValues.append(v)

        print("fichier 1 :", self.file1_name)
        print("fichier 2 :", self.file2_name)

        #ON PASSE EN PARAMETRES TOUTES LES INFORMATIONS RECUPEREES QUE L'UTILISATEUR A RENTRE
        main(graph1=self.file1_name, graph2=self.file2_name, threshold=float(self.Seuil_entry.get()), titre=self.properties_list[0].get(), genre=self.properties_list[1].get(),note=self.properties_list[4].get(), compositeur=self.properties_list[2].get(), clef=self.properties_list[3].get(), opus=self.properties_list[5].get(), identity=self.mesure_list[0].get(), levenshteinBool=self.mesure_list[1].get(), jaroBool=self.mesure_list[2].get(), ngramBool=self.mesure_list[3].get(), ngram_size=int(self.ngram_size.get()) if self.ngram_size.get() else 0, jaccardBool=self.mesure_list[4].get(), monge_elkanBool=self.mesure_list[5].get())
        
        seuil=self.Seuil_entry.get()
        #ET A LA FONCTION DE TESTS DE VERITE DE TERRAIN ON PASSE LE SEUIL AINSI QUE LES 2 FICHIERS SOURCE ET TARGET PSQ ON EN A BESOIN POUR LES VRAIS NEGATIFS
        verite_de_terrain(seuil,source=self.file1_name, target=self.file2_name)
        
        print("threshold = "+self.Seuil_entry.get())
        print("title = "+str(self.properties_list[0].get()))
        print("genre = "+str(self.properties_list[1].get()))
        print("composer = "+str(self.properties_list[2].get()))
        print("key = "+str(self.properties_list[3].get()))
        print("note = "+str(self.properties_list[4].get()))
        print("opus = "+str(self.properties_list[5].get()))
        print("identity = "+str(self.mesure_list[0].get()))
        print("levenshteinBool = "+str(self.mesure_list[1].get()))
        print("jaroBool = "+str(self.mesure_list[2].get()))
        print("ngramBool = "+str(self.mesure_list[3].get()))
        print("ngram_size = "+self.ngram_size.get())
        print("jaccardBool = "+str(self.mesure_list[4].get()))
        print("monge_elkanBool = "+str(self.mesure_list[5].get()))



root = customtkinter.CTk()
root.geometry("500x500")  # Taille de la fenêtre
app = SimilaritySelection(root)
root.mainloop()